using System;

public class CommandLine
{
   public static void Main(string[] args)
   {
       /*Let's try to print what parameters were passed to this program!*/
      Console.WriteLine("Number of command line parameters = {0}", args.Length);
      
      for(int ii = 0; ii < args.Length; ii++)
      {
         Console.WriteLine("Args[{0}] = [{1}]", ii, args[ii]);
      }
      
      /*or you can do it like this
      foreach(string s in args)
      {
         Console.WriteLine(s);
      }*/
   }
}
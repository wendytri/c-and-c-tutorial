#include <fstream>
#include <iostream>
using namespace std;

int main()
{
   char data[100];
   
   //open a file in write mode
   ofstream outfile;
   outfile.open("output.dat");
   
   cout << "Writing to a file..." << endl;
   cout << "Enter your full name: ";
   cin.getline(data, 100);
   
   //write inputted data into the file
   outfile << data << endl;
   
   cout << "Enter your gender: ";
   cin >> data;
   
   //again write inputted data into the file
   outfile << data << endl;
   
   //closed the opened file
   outfile.close();
   
   //open file in read mode
   ifstream infile;
   infile.open("output.dat");
   
   cout << "Reading from the output file..." << endl;
   infile >> data;
   
   //write data at the screen
   cout << data << endl;
   
   //again read the data from the file and display it
   infile >> data;
   cout << data << endl;
   
   //close the opened file
   infile.close();
   
   return 0;
}
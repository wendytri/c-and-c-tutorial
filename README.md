### Purpose ###
To have a basic experience and knowledge in C# and C++ programming language as a preparation to work with most of the blockchain platforms. Since the project restricts the environment to Linux distribution, this tutorial is executed in Ubuntu OS.
This repository stores codes that the owner has written for tutorial purposes.

### Setup ###

```
#!Ubuntu

1. Install FlatPak for Ubuntu using terminal as followed:
   sudo add-apt-repository ppa:alexlarsson/flatpak
   sudo apt update
   sudo apt install flatpak

2. Install MonoDevelop via FlatPak:
   flatpak install --user --from https://download.mono-project.com/repo/monodevelop.flatpakref

3. Install Mono's mcs for C# compiler:
   sudo apt install mono-mcs

4. Intall G++ for C++ compiler:
   sudo apt-get install g++
```


### C# ###
Learning the basic of C# which is one of the object-oriented programming languages and has a strong resemblance to Java. It is a part of .NET framework in Windows and Mono in Linux.

Usually, a C# program consists of:
1. Namespace declaration
2. Class
3. Class Methods
4. Class Attributes
5. Main method
6. Statement and Expressions
7. Comments

P.S. C# is case sensitive and unlike Java, program file name could be different from the class name.

Code Example

```
#!C#

using System; /*Namespace declaration like import or include*/
namespace HelloWorldTutorial /*Namespace declaration*/
{
   class Helloworld /*Class*/
   {
      static void Main(string[] args) /*Main class*/
	  {
	     /*Learning C# language - comment*/
	     Console.WriteLine("Hello World"); /*using a method from Console class defined in the System namespace*/
      }
   }
}

C# file should be saved as your-program-name.cs (note the file extension), in the example above: helloworld.cs

To compile, open terminal in Ubuntu and type:
mcs helloworld.cs

If there are no errors the code, it will generate helloworld.exe as the executable file.
Run the program by entering the following command in the terminal:
mono helloworld.exe

```

### C++ ###
C++ supports object-oriented programming, including the four pillars: Encapsulation, Data hiding, Inheritance, and Polymorphism. As the name implies, it is a superset of C.
C++ can be defined as a collection of objects that communicate via invoking each other methods. C++ program generally has:
1. Objects
2. Class
3. Methods
4. Instance Variables

Code Example:

```
#!C++

#include <iostream> /*like import in java or using in C#*/
using namespace std;

// main() is where program execution begins.

int main() {
   cout << "Hello World"; // prints Hello World
   return 0;
}

C++ file should be saved as your-program-name.cpp (note the file extension), in the example above: helloworld.cpp

To compile, open terminal in Ubuntu and type:
g++ -o helloworld helloworld.cpp

If there are no errors the code, it will generate helloworld as the binary executable file.
Run the program by entering the following command in the terminal:
./helloworld
```